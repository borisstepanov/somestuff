<?php

//поля формы
$name = $_POST['name'];
$tel = $_POST['tel'];

//utm-метки
$utm_source = $_POST['utm-source'];
$utm_medium = $_POST['utm-medium'];
$utm_campaign = $_POST['utm-campaign'];
$utm_content = $_POST['utm-content'];
$utm_term = $_POST['utm-term'];

//сквозная аналитика
$trace = $_POST['TRACE'];

//форма обратной связи
$place = $_POST['place'];


if ($_SERVER["REQUEST_METHOD"] == "POST") {


    // формируем URL в переменной $queryUrl
    $queryUrl = 'https://sbstercw.bitrix24.ru/rest/1/dg5f6pz3wv2kfwdj/crm.lead.add.json';

    // формируем параметры для создания лида в переменной $queryData
    $queryData = http_build_query(array(
        'fields' => array(
            'TITLE' => 'Лид для записи на прием',
            'SOURCE_ID' => WEB,
            'TRACE' =>  $trace,
            'UTM_SOURCE' => $utm_source,
            'UTM_MEDIUM' => $utm_medium,
            'UTM_CAMPAIGN' => $utm_campaign,
            'UTM_CONTENT' => $utm_content,
            'UTM_TERM' => $utm_term,
            'NAME' => $name,
            'PHONE' => array(
                "n0" => array(
                    "VALUE" => "$tel",
                    "VALUE_TYPE" => "WORK",
                ),
            ),
        ),
        'params' => array("REGISTER_SONET_EVENT" => "Y")
    ));

    // обращаемся к Битрикс24 при помощи функции curl_exec
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_SSL_VERIFYPEER => 0,
        CURLOPT_POST => 1,
        CURLOPT_HEADER => 0,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $queryUrl,
        CURLOPT_POSTFIELDS => $queryData,
    ));
    $result = curl_exec($curl);
    curl_close($curl);
    $result = json_decode($result, 1);
    if (array_key_exists('error', $result)) echo "Ошибка при сохранении лида: " . $result['error_description'] . "<br/>";


}

//Вывод json
echo json_encode($result, JSON_UNESCAPED_UNICODE);
?>