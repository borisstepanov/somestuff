<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <title>Flat Modal Window</title>
    <script src='https://code.jquery.com/jquery-2.2.4.min.js'></script>
    <style>
        @import url(https://fonts.googleapis.com/css?family=Montserrat:400,700);
        html, body {
            width: 100%;
            height: 100%;
            margin: 0;
        }
        .page-wrapper {
            width: 100%;
            height: 100%;
            background-size: cover;
        }
        a.btn {
            width: 200px;
            padding: 18px 0;
            position: absolute;
            top: 50%;
            left: 50%;
            -webkit-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
            font-family: 'Montserrat', Arial, Helvetica, sans-serif;
            font-weight: 700;
            text-align: center;
            text-decoration: none;
            text-transform: uppercase;
            color: #fff;
            border-radius: 0;
            background: #e2525c;
        }
        .modal-wrapper {
            width: 100%;
            height: 100%;
            position: fixed;
            top: 0;
            left: 0;
            visibility: hidden;
            opacity: 0;
            -webkit-transition: all 0.25s ease-in-out;
            transition: all 0.25s ease-in-out;
            background: rgba(0, 0, 0, 0.5);
        }
        .modal-wrapper.open {
            opacity: 1;
            visibility: visible;
        }
        .modal {
            width: 500px;
            display: block;
            margin: 30% 0 0 -250px;
            position: relative;
            top: 50%;
            left: 50%;
            opacity: 0;
            -webkit-transition: all 0.5s ease-in-out;
            transition: all 0.5s ease-in-out;
            background: #fafafa;
        }
        .modal-wrapper.open .modal {
            margin-top: -200px;
            opacity: 1;
        }
        .head{
            width: 100%;
            height: 100%;
            margin: 0 0 14px;
            padding: 5px 30px;
            overflow: hidden;
            position: absolute;
        }
        .btn-close {
            font-size: 28px;
            display: block;
            float: right;
            color: #fff;
        }
        .modal .content {
            padding: 0 0 20px 0;
        }
        .good-job {
            text-align: center;
            font-family: 'Montserrat', Arial,       Helvetica, sans-serif;
            color: #e2525c;
        }
        .good-job .fa-thumbs-o-up {
            font-size: 60px;
        }
        .good-job h1 {
            font-size: 45px;
        }
        .form_modal_window{
            max-width: 70%;
            display: flex;
            flex-direction: column;
            margin: auto;
        }
        .modal .content .text{
            background: url("../Bright-Purple-Wallpaper-59-images.jpg");
            color: #fff;
            padding: 10px;
            margin: 20px 0;
        }
        .modal .content .text h2{
            margin:0;
            font-size: 35px;
        }
        .modal .content .text p{
            margin:0;
            font-size: 14px;
        }
        .modal-wrapper{
            margin: 0 0 20px 0;
        }
        .modal .content form input {
            color: #000;
            padding: 15px;
            border: none;
            margin-bottom: 15px;
            box-shadow: 0px 1px 3px 1px #e9e9e9;
            font-size: 15px;
        }
        .modal .content form input[type=submit]{
            padding: 10px 40px;
            max-width: 200px;
            margin: auto;
            border-radius: 54px;
            color: #fff;
            background: #f7b231;
            background: -moz-linear-gradient(top, #ffc281 0%, #ff8c97 100%);
            background: -webkit-linear-gradient(top, #ffc281 0%, #ff8c97 100%);
            background: linear-gradient(to bottom, #ffc281 0%, #ff8c97 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='$color-yellow', endColorstr='$color-pink', GradientType=0);
            font-size: 15px;
            text-shadow: 0px 0px 6px #e54242;
            display: inline-block;
            border: none;
            outline: none;
        }
    </style>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<!-- КНОПКА-->
<div class="page-wrapper">
    <a class="btn trigger" href="#">Открыть форму</a>
</div>

<!-- МОДАЛЬНОЕ ОКНО-->
<div class="modal-wrapper">
    <div class="head"></div>
    <div class="modal">
        <div class="content">
            <div class="good-job">
                <p style="padding: 20px 0 0;color: gray;margin: 0;font-size: 14px;">ОСТАВЬТЕ СВОИ ДАННЫЕ И МЫ С ВАМИ СВЯЖЕМСЯ</p>
                <div class="text">
                    <p>и мы удешевим решение вашей задачи без потери качества</p>
                </div>
                <form class="form_modal_window" action="">
                    <input type="text" name="name" placeholder="Ваше имя" required>
                    <input type="email" name="email" placeholder="Ваш email" required>
                    <input type="phone" name="phone" placeholder="Ваш телефон" required>
                    <input class="form_sub" name="sub" type="submit" value="ОТПРАВИТЬ">
                </form>
                <div id="erconts"></div>
            </div>
        </div>
    </div>
</div>
<!--AJAX СКРИПТ ДЛЯ ФОРМЫ-->
<script>
    $(document).ready(function() {
        $('.form_sub').click(function(){
            $.ajax({
                type: "POST", //указываем что метод отправки POST
                url:"form.php", // указываем адрес обработчика
                data:$('.form_modal_window').serialize(), //указываем данные которые будут передаваться обработчику
                /* Мы указываем id формы - $('#callbacks'), и методом serialize() забираем значения всех полей. */
                error:function(){$("#erconts").html("Произошла ошибка!");},
                /* если произойдет ошибка в элементе с id erconts выведется сообщение*/
                beforeSend: function() {
                    $("#erconts").html("<p style='color: orangered;'>Отправляем данные...</p>");
                },
                success: function(result){
                    /* В случае удачной обработки и отправки выполнится следующий код*/
                    $('#erconts').html(result);
                    checkThis();
                }
            });
            return false;
        });
    });
</script>
<!--СКРИПТ ДЛЯ МОДАЛЬНОГО ОКНА-->
<script>
    $( document ).ready(function() {
        $('.trigger').on('click', function() {
            $('.modal-wrapper').toggleClass('open');
            $('.page-wrapper').toggleClass('blur-it');
            return false;
        });
        $('.head').on('click', function (){
            $('.modal-wrapper').removeClass('open');
        })
    });
</script>


</body>
</html>