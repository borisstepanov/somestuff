<?php

$name = $_POST['name'];
$tel = $_POST['tel'];
$email = $_POST['email'];

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if (empty($name) OR empty($email) OR empty($tel) OR !filter_var($email, FILTER_VALIDATE_EMAIL)) {
        // Set a 400 (bad request) response code and exit.
        http_response_code(400);
        echo "Пожалуйста заполните все поля формы и попробуйте еще раз";
        exit;
    }

    // Send the email.
    if (mail($name, $email, $tel)) {
        // Set a 200 (okay) response code.
        http_response_code(200);
        echo "Ваше сообщение успешно отправлено";

    } else {
        // Set a 500 (internal server error) response code.
        http_response_code(500);
        echo "Что-то пошло не так, и мы не смогли отправить ваше сообщение, попробуйте позже";
    }

    // формируем URL в переменной $queryUrl
    $queryUrl = 'https://sneg.bitrix24.ru/rest/440/plk1o2hzzaczev2b/crm.lead.add.json';

    // формируем параметры для создания лида в переменной $queryData
    $queryData = http_build_query(array(
        'fields' => array(
            'TITLE' => 'Лид с сайта Sneg_school',
            'NAME' => $name,
            'EMAIL' => array(
                "n0" => array(
                    "VALUE" => "$email",
                    "VALUE_TYPE" => "WORK",
                ),
            ),
            'PHONE' => array(
                "n0" => array(
                    "VALUE" => "$tel",
                    "VALUE_TYPE" => "WORK",
                ),
            ),
        ),
        'params' => array("REGISTER_SONET_EVENT" => "Y")
    ));

    // обращаемся к Битрикс24 при помощи функции curl_exec
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_SSL_VERIFYPEER => 0,
        CURLOPT_POST => 1,
        CURLOPT_HEADER => 0,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $queryUrl,
        CURLOPT_POSTFIELDS => $queryData,
    ));
    $result = curl_exec($curl);
    curl_close($curl);
    $result = json_decode($result, 1);
    if (array_key_exists('error', $result)) echo "Ошибка при сохранении лида: " . $result['error_description'] . "<br/>";

//    $redirect = isset($_SERVER['HTTP_REFERER'])? $_SERVER['HTTP_REFERER']:'redirect-form.html';
//    header("Location: $redirect");
//    exit();

} else {
    // Not a POST request, set a 403 (forbidden) response code.
    http_response_code(403);
    echo "Возникла проблема с отправкой, попробуйте еще раз";
}

?>